/**
 * 
 */
package com.emids.healthinsurance.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.emids.healthinsurance.enums.PossibilityStatusEnum;



/**
 * @author emidstest03
 *
 */
@Entity
@Table(name="tb_current_health")
public class CurrentHealth implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 173396296614446282L;

	@Id
	private Long id;
	
	@Column(name="hypertension")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum hypertension;
	
	@Column(name="blood_pressure")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum bloodPressure;
	
	@Column(name="blood_sugar")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum bloodSugar;
	
	@Column(name="over_weight")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum overWeight;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "c_id", nullable = false)
	private CustomerDetails customerDetails;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PossibilityStatusEnum getHypertension() {
		return hypertension;
	}

	public void setHypertension(PossibilityStatusEnum hypertension) {
		this.hypertension = hypertension;
	}

	public PossibilityStatusEnum getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(PossibilityStatusEnum bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public PossibilityStatusEnum getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(PossibilityStatusEnum bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public PossibilityStatusEnum getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(PossibilityStatusEnum overWeight) {
		this.overWeight = overWeight;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}


	
	

}
