/**
 * 
 */
package com.emids.healthinsurance.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emids.healthinsurance.enums.GenderEnum;
import com.emids.healthinsurance.service.GenericPremiumService;
import com.emids.healthinsurance.utility.ConstantValues;

/**
 * @author emidstest03
 *
 */
@Entity
@Table(name = "tb_customer_details")
public class CustomerDetails implements Serializable, GenericPremiumService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7611958410488902570L;
	
	@Transient
	List<GenericPremiumService> genericPremiumServices = new ArrayList<>(); 
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "c_name")
	private String customerName;

	@Enumerated(EnumType.STRING)
	@Column(name = "c_gender")
	private GenderEnum gender;

	@Column(name = "c_age")
	private int age;
	
	@Column(name = "c_premium")
	private double insurancePremium = ConstantValues.basePremium;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "customerDetails", cascade = CascadeType.ALL)
	private CurrentHealth currentHealth;

	@OneToOne(fetch=FetchType.LAZY,mappedBy = "customerDetails", cascade= CascadeType.ALL)
	private Habits habits;
	

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	public Habits getHabits() {
		return habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}

	public double getInsurancePremium() {
		return insurancePremium;
	}

	public void setInsurancePremium(double insurancePremium) {
		this.insurancePremium = insurancePremium;
	}

	public List<GenericPremiumService> getGenericPremiumServices() {
		return genericPremiumServices;
	}

	public void setGenericPremiumServices(List<GenericPremiumService> genericPremiumServices) {
		this.genericPremiumServices = genericPremiumServices;
	}

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		for (GenericPremiumService genericPremiumService : genericPremiumServices) {
			currentPremiumPrice = genericPremiumService.calculatePremium(currentPremiumPrice);
		}
		return (double)currentPremiumPrice;
	}
}
