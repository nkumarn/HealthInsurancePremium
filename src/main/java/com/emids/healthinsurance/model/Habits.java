/**
 * 
 */
package com.emids.healthinsurance.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.emids.healthinsurance.enums.PossibilityStatusEnum;

/**
 * @author emidstest03
 *
 */
@Entity
@Table(name="tb_habits")
public class Habits implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5081008080133556123L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="smoking")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum smoking;
	
	@Column(name="alcohol")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum alcohol;
	
	@Column(name="daily_exercise")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum dailyExercise;
	
	@Column(name="durgs")
	@Enumerated(EnumType.STRING)
	private PossibilityStatusEnum drugs;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "c_id", nullable = false)
	private CustomerDetails customerDetails;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PossibilityStatusEnum getSmoking() {
		return smoking;
	}

	public void setSmoking(PossibilityStatusEnum smoking) {
		this.smoking = smoking;
	}

	public PossibilityStatusEnum getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(PossibilityStatusEnum alcohol) {
		this.alcohol = alcohol;
	}

	public PossibilityStatusEnum getDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(PossibilityStatusEnum dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public PossibilityStatusEnum getDrugs() {
		return drugs;
	}

	public void setDrugs(PossibilityStatusEnum drugs) {
		this.drugs = drugs;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}
	
	
	
	

}
