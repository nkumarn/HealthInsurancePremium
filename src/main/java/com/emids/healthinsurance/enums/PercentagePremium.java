/**
 * 
 */
package com.emids.healthinsurance.enums;

/**
 * @author emidstest03
 *
 */
@SuppressWarnings("rawtypes")
public enum PercentagePremium implements EnumInterface{
	
	PERCENT10(10),
	PERCENT20(20),
	MALE(2),
	CURRENTHEALTH(1),
	HABITS(3);
	
	
	private int percentage;
	private PercentagePremium(int percentage) {
		this.percentage = percentage;
	}

	@Override
	public Integer value() {
		return percentage;
	}

}
