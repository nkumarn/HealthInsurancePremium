/**
 * 
 */
package com.emids.healthinsurance.enums;

/**
 * @author emidstest03
 *
 */
public interface EnumInterface<T extends Object>{
	public T value();

}
