/**
 * 
 */
package com.emids.healthinsurance.enums;


/**
 * @author emidstest03
 *
 */
@SuppressWarnings("rawtypes")
public enum GenderEnum implements EnumInterface{
	MALE("M"),
	FEMALE("F"),
	OTHERS("O");
	
	private String gender;

	GenderEnum(String gender) {
		this.gender = gender;
	}
	@Override
	public String value() {
		return gender;
	}
	
	
}
