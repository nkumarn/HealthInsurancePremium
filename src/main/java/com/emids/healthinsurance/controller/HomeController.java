/**
 * 
 */
package com.emids.healthinsurance.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nkumarn
 *
 */
@RestController
public class HomeController {

	@RequestMapping(value = "/")
	public String home() {
		return "Welcome to the premium calculator";
	}
}
