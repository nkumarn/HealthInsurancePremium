/**
 * 
 */
package com.emids.healthinsurance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.emids.healthinsurance.form.models.CustomerDetailsFormModel;
import com.emids.healthinsurance.model.CustomerDetails;
import com.emids.healthinsurance.repository.StubCustomerDetails;
import com.emids.healthinsurance.ruleengine.PremiumRules;


/**
 * @author emidstest03
 *
 */
@RestController
@RequestMapping(value = "health/")
public class PremiumController {

	@Autowired
	@Qualifier(value="premiumRulesImpl")
	PremiumRules premiumRulesImpl;
	
	@Autowired
	@Qualifier(value="stubCustomerDetailsImpl")
	StubCustomerDetails stubCustomerDetailsImpl;
	
	@RequestMapping(value = "get")
	public String premium() {
		return "Hi premium";
	}

	@RequestMapping(value = "getpremium", method = RequestMethod.GET)
	public @ResponseBody CustomerDetailsFormModel getPremium() {
		CustomerDetails customerDetails = stubCustomerDetailsImpl.getCustomerDetails();
		return premiumRulesImpl.getCustomerWithPremium(customerDetails);
	}
}
