/**
 * 
 */
package com.emids.healthinsurance.utility;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * @author emidstest03
 *
 */
@Component
public class ConstantValues implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3482400561834910915L;
	
	public static final double basePremium = 5000;
}
