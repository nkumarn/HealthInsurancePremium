/**
 * 
 */
package com.emids.healthinsurance.utility;

import java.io.Serializable;

import org.springframework.stereotype.Component;


/**
 * @author emidstest03
 *
 */
@Component("percentageCal")
public class PercentageCal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6096600213211753566L;
	
	public double increasePercentagePremiumValue(double existingPrimium, double percentage){
		
		return ((existingPrimium * percentage)/100)+existingPrimium;
	}
	
	public double reducePercentagePremiumValue(double currentValue, double percentage){
		
		return currentValue - ((currentValue * percentage)/100);
	}

}
