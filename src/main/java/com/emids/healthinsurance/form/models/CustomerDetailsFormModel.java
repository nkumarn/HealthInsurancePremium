/**
 * 
 */
package com.emids.healthinsurance.form.models;

import com.emids.healthinsurance.enums.GenderEnum;

/**
 * @author nkumarn
 *
 */
public class CustomerDetailsFormModel {
	
	private String name;
	private int age;
	private GenderEnum gender;
	private Double insurancePremium;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public GenderEnum getGender() {
		return gender;
	}
	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}
	public Double getInsurancePremium() {
		return insurancePremium;
	}
	public void setInsurancePremium(Double insurancePremium) {
		this.insurancePremium = insurancePremium;
	}
	@Override
	public String toString() {
		return "CustomerDetailsFormModel [name=" + name + ", age=" + age + ", gender=" + gender + ", insurancePremium="
				+ insurancePremium + "]";
	}
	
	

}
