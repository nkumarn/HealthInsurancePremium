/**
 * 
 */
package com.emids.healthinsurance.repository.impl;

import org.springframework.stereotype.Repository;

import com.emids.healthinsurance.enums.GenderEnum;
import com.emids.healthinsurance.enums.PossibilityStatusEnum;
import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.CustomerDetails;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurance.repository.StubCustomerDetails;

/**
 * @author nkumarn
 *
 */
@Repository("stubCustomerDetailsImpl")
public class StubCustomerDetailsImpl implements StubCustomerDetails{

	@Override
	public CustomerDetails getCustomerDetails(){
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setId(1L);
		customerDetails.setCustomerName("Norman Gomes");
		customerDetails.setGender(GenderEnum.MALE);
		customerDetails.setAge(34);

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setId(1L);
		currentHealth.setHypertension(PossibilityStatusEnum.NO);
		currentHealth.setBloodSugar(PossibilityStatusEnum.NO);
		currentHealth.setBloodPressure(PossibilityStatusEnum.NO);
		currentHealth.setOverWeight(PossibilityStatusEnum.YES);

		customerDetails.setCurrentHealth(currentHealth);

		Habits habits = new Habits();
		habits.setId(1L);
		habits.setSmoking(PossibilityStatusEnum.NO);
		habits.setAlcohol(PossibilityStatusEnum.YES);
		habits.setDailyExercise(PossibilityStatusEnum.YES);
		habits.setDrugs(PossibilityStatusEnum.NO);

		customerDetails.setHabits(habits);
		return customerDetails;
	}

}
