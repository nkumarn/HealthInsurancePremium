/**
 * 
 */
package com.emids.healthinsurance.repository;

import com.emids.healthinsurance.model.CustomerDetails;

/**
 * @author nkumarn
 *
 */
public interface StubCustomerDetails {
	
	public CustomerDetails getCustomerDetails();
	
}
