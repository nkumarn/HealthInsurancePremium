/**
 * 
 */
package com.emids.healthinsurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emids.healthinsurance.model.CustomerDetails;

/**
 * @author nkumarn
 *
 */
@Repository
public interface CustomerDetailsRepo extends JpaRepository<CustomerDetails, Long> {

}
