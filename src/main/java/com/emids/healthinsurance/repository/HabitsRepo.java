/**
 * 
 */
package com.emids.healthinsurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emids.healthinsurance.model.Habits;

/**
 * @author nkumarn
 *
 */
@Repository
public interface HabitsRepo extends JpaRepository<Habits, Long>{

}
