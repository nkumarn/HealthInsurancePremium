/**
 * 
 */
package com.emids.healthinsurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emids.healthinsurance.model.CurrentHealth;

/**
 * @author nkumarn
 *
 */
@Repository
public interface CurrentHealthRepo extends JpaRepository<CurrentHealth, Long>{

}
