/**
 * 
 */
package com.emids.healthinsurance.CustomExceptions;

/**
 * @author nkumarn
 *
 */
public class InvalidDataException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8296952525215028300L;

	public InvalidDataException(String str) {
		super(str);
	}
}
