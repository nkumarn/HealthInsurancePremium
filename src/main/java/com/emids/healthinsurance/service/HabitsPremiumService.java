/**
 * 
 */
package com.emids.healthinsurance.service;


import com.emids.healthinsurance.model.Habits;

/**
 * @author emidstest03
 *
 */
public interface HabitsPremiumService extends GenericPremiumService {
	
	public double increasePremiumByHabits(Habits habits, double existingPremium) ;

}
