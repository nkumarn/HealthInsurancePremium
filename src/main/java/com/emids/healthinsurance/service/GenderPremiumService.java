/**
 * 
 */
package com.emids.healthinsurance.service;



/**
 * @author emidstest03
 *
 */
public interface GenderPremiumService extends GenericPremiumService {

	public double increasePremiumByGender(String gender, double existingPremium) ;
}
