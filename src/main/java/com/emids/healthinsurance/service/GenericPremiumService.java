/**
 * 
 */
package com.emids.healthinsurance.service;


/**
 * @author emidstest03
 *
 */
public interface GenericPremiumService {

	public double calculatePremium(Double currentPremiumPrice);
}
