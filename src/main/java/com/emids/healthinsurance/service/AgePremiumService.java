/**
 * 
 */
package com.emids.healthinsurance.service;



/**
 * @author emidstest03
 *
 */
public interface AgePremiumService extends GenericPremiumService{

	public double increasePremiumByAge(int age, double existingPrimium);
}
