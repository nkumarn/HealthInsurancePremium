package com.emids.healthinsurance.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.service.GenericPremiumService;

@Service
@Lazy
public class GenericPremiumServiceImpl implements GenericPremiumService {

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		return 0;
	}

}
