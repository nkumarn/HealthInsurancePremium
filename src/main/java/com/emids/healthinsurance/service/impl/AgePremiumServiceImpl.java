/**
 * 
 */
package com.emids.healthinsurance.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.enums.PercentagePremium;
import com.emids.healthinsurance.service.AgePremiumService;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author emidstest03
 *
 */
@Service
@Lazy
public class AgePremiumServiceImpl implements AgePremiumService {

	
	private PercentageCal percentageCal = new PercentageCal();
	
	private int age;
	
	
	public AgePremiumServiceImpl(int age) {
		super();
		this.age = age;
	}
	 
	
	@Override
	public double increasePremiumByAge(int age, double  existingPrimium) {
		if(age>18 && age<25) {
			return percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value());
		}else if(age>25 && age <30) {
			return percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value());
		}else if(age>30 && age < 35) {
			return percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value());
		}else if(age>35 && age < 40) {
			return percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value());
		}else if(age>40&&age < 45) {
			return percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT20.value());
		}else if(age>45) {
			existingPrimium = percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT10.value()), PercentagePremium.PERCENT20.value());
			int tempAge = age - 40;
			int timesOfPercenatage = (int) (Math.ceil(tempAge / 5.0));
			for(int i=0;i<timesOfPercenatage;i++){
				existingPrimium = percentageCal.increasePercentagePremiumValue(existingPrimium, PercentagePremium.PERCENT20.value());
			}
			return existingPrimium;
		}else if(age<18) {
			return existingPrimium;
		}
		return 0;
	}

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		try{
		return increasePremiumByAge(age, currentPremiumPrice);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return currentPremiumPrice;
	}
}
