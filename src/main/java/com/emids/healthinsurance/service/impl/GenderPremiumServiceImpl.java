/**
 * 
 */
package com.emids.healthinsurance.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.enums.GenderEnum;
import com.emids.healthinsurance.enums.PercentagePremium;
import com.emids.healthinsurance.service.GenderPremiumService;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author emidstest03
 *
 */
@Service
@Lazy
public class GenderPremiumServiceImpl implements GenderPremiumService {

	private PercentageCal percentageCal = new PercentageCal();

	String gender;

	public GenderPremiumServiceImpl(String gender) {
		super();
		this.gender = gender;
	}

	@Override
	public double increasePremiumByGender(String gender, double existingPremium) {
		if (gender.equals(GenderEnum.MALE.value())) {
			return percentageCal.increasePercentagePremiumValue(existingPremium, PercentagePremium.MALE.value());
		} else {
			return existingPremium;
		}

	}

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		try {
			return increasePremiumByGender(gender, currentPremiumPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentPremiumPrice;
	}

}
