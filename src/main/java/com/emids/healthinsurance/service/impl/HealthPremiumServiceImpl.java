/**
 * 
 */
package com.emids.healthinsurance.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.enums.PercentagePremium;
import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.service.HealthPremiumService;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author emidstest03
 *
 */
@Service
@Lazy
public class HealthPremiumServiceImpl implements HealthPremiumService {

	private PercentageCal percentageCal = new PercentageCal();

	CurrentHealth currentHealth;

	public HealthPremiumServiceImpl(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	@Override
	public double increasePremiumByHealthCondition(CurrentHealth currentHealth, double existingPremium) {
		if (currentHealth.getBloodPressure().name().equals("YES")) {
			return percentageCal.increasePercentagePremiumValue(existingPremium,
					PercentagePremium.CURRENTHEALTH.value());
		}
		if (currentHealth.getBloodSugar().name().equals("YES")) {
			return percentageCal.increasePercentagePremiumValue(existingPremium,
					PercentagePremium.CURRENTHEALTH.value());
		}
		if (currentHealth.getHypertension().name().equals("YES")) {
			return percentageCal.increasePercentagePremiumValue(existingPremium,
					PercentagePremium.CURRENTHEALTH.value());
		}
		if (currentHealth.getOverWeight().name().equals("YES")) {
			return percentageCal.increasePercentagePremiumValue(existingPremium,
					PercentagePremium.CURRENTHEALTH.value());
		}
			return existingPremium;
	}

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		try {
			return increasePremiumByHealthCondition(currentHealth, currentPremiumPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentPremiumPrice;
	}

}
