/**
 * 
 */
package com.emids.healthinsurance.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.enums.PercentagePremium;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurance.service.HabitsPremiumService;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author emidstest03
 *
 */
@Service
@Lazy
public class HabitsPremiumImpl implements HabitsPremiumService {
	private PercentageCal percentageCal = new PercentageCal();

	Habits habits;

	public HabitsPremiumImpl(Habits habits) {
		super();
		this.habits = habits;
	}

	@Override
	public double increasePremiumByHabits(Habits habits, double existingPremium) {

		if (habits.getDailyExercise().name().equals("YES")) {
			existingPremium = percentageCal.increasePercentagePremiumValue(existingPremium,
					PercentagePremium.HABITS.value());
		}
		if (habits.getAlcohol().name().equals("YES")) {
			existingPremium = percentageCal.reducePercentagePremiumValue(existingPremium,
					PercentagePremium.HABITS.value());
		}
		if (habits.getSmoking().name().equals("YES")) {
			existingPremium = percentageCal.reducePercentagePremiumValue(existingPremium,
					PercentagePremium.HABITS.value());
		}
		if (habits.getDrugs().name().equals("YES")) {
			existingPremium = percentageCal.reducePercentagePremiumValue(existingPremium,
					PercentagePremium.HABITS.value());
		}
		return existingPremium;

	}

	@Override
	public double calculatePremium(Double currentPremiumPrice) {
		try {
			return increasePremiumByHabits(habits, currentPremiumPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentPremiumPrice;
	}

}
