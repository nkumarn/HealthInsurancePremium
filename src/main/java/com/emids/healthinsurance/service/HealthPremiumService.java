/**
 * 
 */
package com.emids.healthinsurance.service;


import com.emids.healthinsurance.model.CurrentHealth;

/**
 * @author emidstest03
 *
 */
public interface HealthPremiumService extends GenericPremiumService{
	
	public double increasePremiumByHealthCondition(CurrentHealth currentHealth, double existingPremium) ;

}
