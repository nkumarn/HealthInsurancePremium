/**
 * 
 */
package com.emids.healthinsurance.ruleengine;

import com.emids.healthinsurance.form.models.CustomerDetailsFormModel;
import com.emids.healthinsurance.model.CustomerDetails;

/**
 * @author nkumarn
 *
 */
public interface PremiumRules {
	
	public CustomerDetailsFormModel getCustomerWithPremium(CustomerDetails customerDetails);

}
