/**
 * 
 */
package com.emids.healthinsurance.ruleengine.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.emids.healthinsurance.form.models.CustomerDetailsFormModel;
import com.emids.healthinsurance.model.CustomerDetails;
import com.emids.healthinsurance.ruleengine.PremiumRules;
import com.emids.healthinsurance.service.impl.AgePremiumServiceImpl;
import com.emids.healthinsurance.service.impl.GenderPremiumServiceImpl;
import com.emids.healthinsurance.service.impl.HabitsPremiumImpl;
import com.emids.healthinsurance.service.impl.HealthPremiumServiceImpl;
import com.emids.healthinsurance.utility.ConstantValues;

/**
 * @author nkumarn
 *
 */
@Service("premiumRulesImpl")
@Lazy
public class PremiumRulesImpl implements PremiumRules{

	@Override
	public CustomerDetailsFormModel getCustomerWithPremium(CustomerDetails customerDetails) {
		customerDetails.getGenericPremiumServices().add(new AgePremiumServiceImpl(customerDetails.getAge()));
		customerDetails.getGenericPremiumServices()
				.add(new GenderPremiumServiceImpl(customerDetails.getGender().value()));
		customerDetails.getGenericPremiumServices()
				.add(new HealthPremiumServiceImpl(customerDetails.getCurrentHealth()));
		customerDetails.getGenericPremiumServices().add(new HabitsPremiumImpl(customerDetails.getHabits()));
		customerDetails.setInsurancePremium(customerDetails.calculatePremium(ConstantValues.basePremium));
		
		//Send only the basic details
		CustomerDetailsFormModel customerDetailsFormModel = new CustomerDetailsFormModel();
		customerDetailsFormModel.setAge(customerDetails.getAge());
		customerDetailsFormModel.setGender(customerDetails.getGender());
		customerDetailsFormModel.setName(customerDetails.getCustomerName());
		customerDetailsFormModel.setInsurancePremium(customerDetails.getInsurancePremium());
		return customerDetailsFormModel;
	}

}
