/**
 * 
 */
package com.emids.healthinsurance.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.emids.healthinsurance.enums.PossibilityStatusEnum;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurance.service.HabitsPremiumService;
import com.emids.healthinsurance.service.impl.HabitsPremiumImpl;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author nkumarn
 *
 */
@SpringBootTest
public class HabitsPremiumServiceImplTest {
	
	private HabitsPremiumService habitsPremiumService;
	
	@Mock
	PercentageCal percentageCal;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testPremiumForDailyExercise() {
		double value = 0;
		// Mocking the object
		when(percentageCal.increasePercentagePremiumValue(100.0, 3.0)).thenReturn(103.0);
		Habits habits = new Habits();
		habits.setAlcohol(PossibilityStatusEnum.NO);
		habits.setDailyExercise(PossibilityStatusEnum.YES);
		habits.setDrugs(PossibilityStatusEnum.NO);
		habits.setSmoking(PossibilityStatusEnum.NO);
		habitsPremiumService = new HabitsPremiumImpl(habits);
		value = habitsPremiumService.calculatePremium(100.0);
		int temp = (int)value;
		assertEquals(103, temp);
	}
	
	@Test
	public void testPremiumForSmoking() {
		double value = 0;
		// Mocking the object
		when(percentageCal.increasePercentagePremiumValue(100.0, 3.0)).thenReturn(97.0);
		Habits habits = new Habits();
		habits.setAlcohol(PossibilityStatusEnum.YES);
		habits.setDailyExercise(PossibilityStatusEnum.NO);
		habits.setDrugs(PossibilityStatusEnum.NO);
		habits.setSmoking(PossibilityStatusEnum.NO);
		habitsPremiumService = new HabitsPremiumImpl(habits);
		value = habitsPremiumService.calculatePremium(100.0);
		int temp = (int)value;
		System.out.println(temp);
		assertEquals(97, temp);
	}

}
