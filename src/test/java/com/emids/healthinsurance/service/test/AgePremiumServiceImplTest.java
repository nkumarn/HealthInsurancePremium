package com.emids.healthinsurance.service.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.emids.healthinsurance.service.AgePremiumService;
import com.emids.healthinsurance.service.impl.AgePremiumServiceImpl;
import com.emids.healthinsurance.utility.PercentageCal;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * @author nkumarn
 *
 */
@SpringBootTest
public class AgePremiumServiceImplTest {

	private AgePremiumService agePremiumService;

	@Mock
	PercentageCal percentageCal;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testIncreasePremiumByAge() {
		double value = 0;
		// Mocking the object
		when(percentageCal.increasePercentagePremiumValue(5000.0, 10.0)).thenReturn(5500.0);
		agePremiumService = new AgePremiumServiceImpl(19);
		value = agePremiumService.calculatePremium(5000.0);
		int temp = (int)value;
		assertEquals(5500, temp);
	}
}
