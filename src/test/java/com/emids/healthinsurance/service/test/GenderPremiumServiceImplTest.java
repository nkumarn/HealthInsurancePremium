/**
 * 
 */
package com.emids.healthinsurance.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.emids.healthinsurance.enums.GenderEnum;
import com.emids.healthinsurance.service.GenderPremiumService;
import com.emids.healthinsurance.service.impl.GenderPremiumServiceImpl;
import com.emids.healthinsurance.utility.PercentageCal;

/**
 * @author nkumarn
 *
 */
@SpringBootTest
public class GenderPremiumServiceImplTest {
	
	private GenderPremiumService genderPremiumService;
	
	@Mock
	PercentageCal percentageCal;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testIncreasePremiumByGender() {
		double value = 0;
		// Mocking the object
		when(percentageCal.increasePercentagePremiumValue(100.0, 2.0)).thenReturn(102.0);
		genderPremiumService = new GenderPremiumServiceImpl(GenderEnum.MALE.value());
		value = genderPremiumService.calculatePremium(100.0);
		int temp = (int)value;
		assertEquals(102, temp);
	}

}
