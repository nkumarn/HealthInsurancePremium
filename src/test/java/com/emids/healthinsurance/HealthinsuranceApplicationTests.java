package com.emids.healthinsurance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.emids.healthinsurance.controller.HomeController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HealthinsuranceApplicationTests {

	@Test
	public void testApp() {
		HomeController homeController = new HomeController();
		String result = homeController.home();
		assertEquals(result,"Welcome to the premium calculator");
	}

}
