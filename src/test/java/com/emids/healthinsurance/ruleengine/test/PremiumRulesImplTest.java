/**
 * 
 */
package com.emids.healthinsurance.ruleengine.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.emids.healthinsurance.enums.GenderEnum;
import com.emids.healthinsurance.enums.PossibilityStatusEnum;
import com.emids.healthinsurance.form.models.CustomerDetailsFormModel;
import com.emids.healthinsurance.model.CurrentHealth;
import com.emids.healthinsurance.model.CustomerDetails;
import com.emids.healthinsurance.model.Habits;
import com.emids.healthinsurance.ruleengine.PremiumRules;
import com.emids.healthinsurance.ruleengine.impl.PremiumRulesImpl;

/**
 * @author nkumarn
 *
 */
@SpringBootTest
public class PremiumRulesImplTest {
	
	@Mock
	PremiumRules premiumRulesImpl;
	
	

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	
	@Test
	public void testPremiumController(){
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setId(1L);
		customerDetails.setCustomerName("Nitesh");
		customerDetails.setGender(GenderEnum.MALE);
		customerDetails.setAge(27);

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setId(1L);
		currentHealth.setHypertension(PossibilityStatusEnum.NO);
		currentHealth.setBloodSugar(PossibilityStatusEnum.NO);
		currentHealth.setBloodPressure(PossibilityStatusEnum.NO);
		currentHealth.setOverWeight(PossibilityStatusEnum.YES);

		customerDetails.setCurrentHealth(currentHealth);

		Habits habits = new Habits();
		habits.setId(1L);
		habits.setSmoking(PossibilityStatusEnum.NO);
		habits.setAlcohol(PossibilityStatusEnum.YES);
		habits.setDailyExercise(PossibilityStatusEnum.YES);
		habits.setDrugs(PossibilityStatusEnum.NO);

		customerDetails.setHabits(habits);
		
		
		CustomerDetailsFormModel customerDetailsFormModel = new PremiumRulesImpl().getCustomerWithPremium(customerDetails);
		int insurancePremium = customerDetailsFormModel.getInsurancePremium().intValue();
		assertEquals(6227, insurancePremium);
		
		
	}
	

}
